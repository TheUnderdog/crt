function CreateCtrlList()
{
	var TempClass = {
						InternalList:[],
						GetLength:function()
						{
							return this.InternalList.length;
						},
						IsAppendEndUnique:function(IncomingObject)
						{
							if(this.InternalList.length < 1)
							{
								return true;
							}
							
							return this.InternalList[this.InternalList.length-1] != IncomingObject;
						},
						AppendEndUnique:function(IncomingObject)
						{
							if(this.IsAppendEndUnique(IncomingObject))
							{
								this.InternalList.push(IncomingObject);
								return true;
							}
							return false;
						},
						Pop:function()
						{
							if(this.GetLength() > 0)
							{
								return this.InternalList.pop();
							}
							else
							{
								return "";
							}
						},
						PopSwap:function(IncomingCtrlList)
						{
							if(this.InternalList.length > 0)
							{
								
								var Popped = this.InternalList.pop();
								IncomingCtrlList.AppendEndUnique(Popped);
								return Popped;
							}
							
							return "";
						},
						UpdateClear:function(OptionalLookAhead)
						{
							if(this.InternalList.length > 0)
							{
								if(OptionalLookAhead !== undefined)
								{
									this.InternalList = [];
								}
								else if(this.InternalList[this.InternalList.length-1] == OptionalLookAhead)
								{
									this.InternalList.pop();
								}
								else
								{
									this.InternalList = [];
								}
							}
						}
					};
	return TempClass;
}

function CreateUndoClass(TargetCurrent)
{
	var TempClass = {
						UndoList:null,
						Current:"",
						RedoList:null,
						
						GetCurrent:function(){return this.Current;},
						
						UpdateCurrent:function(IncomingObject)
						{
							this.UndoList.AppendEndUnique(this.Current);
							this.Current = IncomingObject;
							this.RedoList.UpdateClear(IncomingObject);
						},
						Undo:function()
						{
							if(this.UndoList.GetLength() > 0)
							{
								this.RedoList.AppendEndUnique(this.Current);
								this.Current = this.UndoList.Pop();
							}
							
							return this.Current;
						},
						Redo:function()
						{
							if(this.RedoList.GetLength() > 0)
							{
								this.UndoList.AppendEndUnique(this.Current);
								this.Current = this.RedoList.Pop();
							}
							return this.Current;
						}
					};
	
	TempClass.UndoList = CreateCtrlList();
	TempClass.RedoList = CreateCtrlList();
	TempClass.Current = TargetCurrent;
	return TempClass;
}

var UndoClass = CreateUndoClass(TempTextArea.value);
