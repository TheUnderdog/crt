function IsInvalid(T)	{ return (T === undefined) || (T === null); }
function IsValid(T)		{ return !IsInvalid(T); }

function IsString1(T)	{ return (typeof T === 'string') || (T instanceof String); }
function IsString(T)	{ return IsValid(T) && (IsString1(T)); }
function IsNotString(T)	{ return !IsString(T); }

function IsArray(T){ return IsValid(T) && Array.isArray(T); }

function IsDict(T){ return typeof T === 'object' && !IsArray(T);}

function IsBoolean(T){ return typeof(T) === 'boolean'; }
function IsInt(T){ return Number.isInteger(T); }
function IsIntSafe(T){ return Number.isSafeInteger(T); }
function IsFloat(T){ return Number(T) === T && T % 1 !== 0; }
function IsLong(T){ return T > Number.MAX_SAFE_INTEGER || T < Number.MIN_SAFE_INTEGER; };

function IsPOD(T){ return IsBoolean(T) || IsInt(T) || IsIntSafe(T) || IsFloat(T) || IsLong(T); }
