function SetCaretPos(IncomingElement, IncomingPos)
{
	if(IncomingElement != null)
	{
		if(IncomingElement.createTextRange)
		{
			var TempRange = IncomingElement.createTextRange();
			TempRange.move('character', IncomingPos);
			TempRange.select();
		}
		else
		{
			if(IncomingElement.selectionStart)
			{
				IncomingElement.focus();
				IncomingElement.setSelectionRange(IncomingPos, IncomingPos);
			}
			else
			{
				IncomingElement.focus();
			}
		}
	}
}

function GetCaretPosition(IncomingElement)
{ 
	return (IncomingElement.selectionStart) ? IncomingElement.selectionStart : IncomingElement.value.length;
}

function InsertAtCursor(myField, myValue)
{
	if(myField.selectionStart || myField.selectionStart == '0')
	{
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)
			+ myValue 
			+ myField.value.substring(endPos, myField.value.length);

		myField.selectionStart = startPos + myValue.length;
		myField.selectionEnd = myField.selectionStart;
	}
	else
	{
		myField.value += myValue;
	}
}
