function PopulateEmojis()
{
	var Temp = '💉🧬☠💀🪦📈📉💰📰📞💔🫀🫁🧠🖕⛔❌🏥⚰🕯🤡💩🤖';
	
	TempList = UnicodeHandler.GetAddStringUnique(Temp);
	
	var UEID = document.getElementById("UsefulEmojis");
	
	for(var Iter = 0;Iter < TempList.length;Iter++)
	{
		AppendCharacterToElement(TempList[Iter],UEID);
	}
}

PopulateEmojis();
