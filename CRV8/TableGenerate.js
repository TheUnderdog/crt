function GenerateTable()
{
	if(document.getElementById("SubstitutionEnabled").checked)
	{
		var TempTable = {};
		var RotationNeutralLoaded = false;
		
		if(document.getElementById("StandardEnabled").checked)
		{
			if(document.getElementById("NeutralEnabled").checked)
			{
				TempTable = MergeCopyTable(NonRotationNeutralTable,RotationNeutralTable);
				RotationNeutralLoaded = true;
			}
			else
			{
				TempTable = MergeCopyTable(TempTable,NonRotationNeutralTable);
			}
		}
		
		if(document.getElementById("BlackBoxEnabled").checked)
		{
			TempTable = MergeCopyTable(TempTable,BlackBoxTable);
		}
		
		if(document.getElementById("CircleEnabled").checked)
		{
			TempTable = MergeCopyTable(TempTable,CircleTable);
		}
		
		if(document.getElementById("FancyEnabled").checked)
		{
			TempTable = MergeCopyTable(TempTable,FancyTable);
		}
		
		if(document.getElementById("SuperScriptEnabled").checked)
		{
			if(document.getElementById("NeutralEnabled").checked)
			{
				TempTable = MergeCopyTable(TempTable,SuperScriptNonNeutralRotationTable);
				TempTable = MergeCopyTable(TempTable,SuperScriptNeutralRotationTable);
			}
			
			if(document.getElementById("ReversedEnabled").checked)
			{
				TempTable = MergeCopyTable(TempTable,SuperScriptReversed);
			}
			
			TempTable = MergeCopyTable(TempTable,SuperScriptNonNeutralRotationTable);
		}
		
		if(document.getElementById("ReversedEnabled").checked)
		{
			if(!RotationNeutralLoaded && document.getElementById("NeutralEnabled").checked)
			{
				TempTable = MergeCopyTable(TempTable,RotationNeutralTable);
			}
			
			TempTable = MergeCopyTable(TempTable,ReversedTable);
		}
		
		return TempTable
	}
	else
	{
		return {};
	}
}
