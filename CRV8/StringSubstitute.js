function GetSelectedText()
{
	if(TempTextArea.selectionStart !== undefined)
	{
		return TempTextArea.value.substring(TempTextArea.selectionStart, TempTextArea.selectionEnd);
	}
	return null;
}

function InsertInString(IncomingString, IncomingIndex, StringInsert)
{
	if(IncomingIndex >= (IncomingString.length-1))
	{
		return IncomingString += StringInsert;
	}
	else
	{
		return IncomingString.substr(0, IncomingIndex) + StringInsert + IncomingString.substr(IncomingIndex);
	}
}

function SubstituteString(IncomingString)
{
	var ToList = UnicodeHandler.ConvertUnicodeStringToList(IncomingString);
	var TempList = null, Rand = null;
	
	for(Key in MultiTable)
	{
		TempList = UnicodeHandler.GetAddStringUnique(MultiTable[Key]);
		for(var Iter = 0;Iter < ToList.length;++Iter)
		{
			if(ToList[Iter] === Key)
			{
				Rand = GetRand(0,TempList.length-1)
				ToList[Iter] = TempList[Rand];
			}
		}
	}
	
	var ProbE = document.getElementById('Probability');
	var TempProb = parseFloat(ProbE.value);
	
	for(Key in GlobalTable)
	{
		TempList = UnicodeHandler.GetAddStringUnique(GlobalTable[Key]);
		var LMO = TempList.length-1;
		for(var Iter = 0;Iter < ToList.length;++Iter)
		{
			if(ToList[Iter] === Key)
			{
				if(Math.random() < TempProb)
				{
					Rand = GetRand(0,LMO)
					ToList[Iter] = TempList[Rand];
				}
			}
		}
	}
	
	return UnicodeHandler.ConvertListToUnicodeString(ToList);
}

function SubstituteSelectedText()
{
	if(TempTextArea.selectionStart !== undefined)
	{
		var selectedText;
		var StartPos = TempTextArea.selectionStart;
		var EndPos = TempTextArea.selectionEnd;
		
		Substitution = SubstituteString(TempTextArea.value.substring(StartPos, EndPos))
		
		TempTextArea.value = TempTextArea.value.substring(0, StartPos) + Substitution + TempTextArea.value.substring(EndPos, TempTextArea.value.length);
	}
}
