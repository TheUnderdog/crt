document.getElementById("StandardEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
document.getElementById("CircleEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
document.getElementById("ReversedEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
document.getElementById("NeutralEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
document.getElementById("BlackBoxEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
document.getElementById("FancyEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
document.getElementById("SuperScriptEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
document.getElementById("SingleEnabled").addEventListener("change", function(){ GlobalTable = GenerateTable(); });
		
TempTextArea.addEventListener('keyup', function(e)
{
	if(e.ctrlKey)
	{
		//Ctrl Z or Ctrl Y
		if(e.keyCode == 90 || e.keyCode == 89)
		{
			return true;
		}
	}
	
	UndoClass.UpdateCurrent(TempTextArea.value);
}
);

TempTextArea.addEventListener('keydown', function(e)
{
	//Ctrl + V
	if(e.ctrlKey && e.keyCode == 86)
	{
		return true;
	}
	
	//Ctrl + Y
	if(e.ctrlKey && e.keyCode == 89)
	{
		TempTextArea.value = UndoClass.Redo();
		e.preventDefault();
		return false;
	}
	
	//Ctrl + Z
	if(e.ctrlKey && e.keyCode == 90)
	{
		TempTextArea.value = UndoClass.Undo();
		e.preventDefault();
		return false;
	}
	
	//Ctrl + S
	if(e.ctrlKey && e.keyCode == 83)
	{
		SubstituteSelectedText();
		e.preventDefault();
		return false;
	}
	
	if(!document.getElementById("SubstitutionEnabled").checked)
	{
		return true;
	}
	
	return SubstituteKeyCode(e);
});
