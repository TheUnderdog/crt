function CopyList(IncomingList)
{
	var NewList = [];
	for(var Iter = 0;Iter < IncomingList.length;Iter++)
	{
		if(IsString(IncomingList[Iter]))
		{
			NewList[Iter] = ""+IncomingList[Iter];
		}
		else if(IsPOD(IncomingList[Iter]))
		{
			NewList[Iter] = IncomingList[Iter];
		}
		else if(IsArray(IncomingList[Iter]))
		{
			NewList[Iter] = CopyList(IncomingList[Iter]);
		}
		else if(IsDict(IncomingList[Iter]))
		{
			NewList[Iter] = CopyDict(IncomingList[Iter]);
		}
		else
		{
			console.log("Unrecognised List Subtype: "+IncomingList[Iter]);
		}
	}
	
	return NewList;
}

function CopyDict(IncomingDict)
{
	var NewDict = {};
	for(Key in IncomingDict)
	{
		if(IsString(IncomingDict[Key]))
		{
			NewDict[Key] = ""+IncomingDict[Key];
		}
		else if(IsPOD(IncomingDict[Key]))
		{
			NewDict[Key] = IncomingDict[Key];
		}
		else if(IsArray(IncomingDict[Key]))
		{
			NewDict[Key] = CopyList(IncomingDict[Key]);
		}
		else if(IsDict(IncomingDict[Key]))
		{
			NewDict[Key] = CopyDict(IncomingDict[Key]);
		}
		else
		{
			console.log("Unrecognised Dict Subtype: "+IncomingDict[Key]);
		}
	}
	
	return NewDict;
}

function MergeCopyTable(TargetTable,SourceTable)
{
	if(TargetTable === null)
	{
		return CopyDict(SourceTable);
	}
	else if(TargetTable.length < 1)
	{
		return CopyDict(SourceTable);
	}
	else
	{
		var TempTable = CopyDict(TargetTable);
		var TempTableKeys = Object.keys(TempTable);
		
		for(Key in SourceTable)
		{
			if(TempTableKeys.indexOf(Key) < 0)
			{
				TempTable[Key] = SourceTable[Key];
				TempTableKeys.push(Key);
			}
			else
			{
				TempTable[Key] = TempTable[Key] + SourceTable[Key];
			}
		}
		return TempTable;
	}
}
