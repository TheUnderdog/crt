//Shim for older JavaScript engines that lack the Object.keys function
if(!Object.keys)
{
	Object.keys = function (object)
	{
		var keys = [];

		for (var key in object)
		{
			if (object.hasOwnProperty(key))
			{
				keys.push(key);
			}
		}
	}
}
