function GetSurrogatePair(IncomingString,Iter)
{
	var SurrStartSignal = 0xd800, SurrEndSignal = 0xdbff;
	var ValidSurrPairStart = 0xdc00, ValidSurrPairEnd = 0xdfff;
	var CCA = IncomingString.charCodeAt(Iter);
	
	if(CCA >= SurrStartSignal && CCA <= SurrEndSignal)
	{
		if( !(Iter < IncomingString.length-1) )
		{
			return null;
		}
		
		CCA = IncomingString.charCodeAt(Iter+1);
		
		if(CCA >= ValidSurrPairStart && CCA <= ValidSurrPairEnd)
		{
			return IncomingString[Iter] + IncomingString[Iter+1];
		}
	}
	
	return null;
}

function IsSurrogatePair(IncomingString,Iter)
{
	return GetSurrogatePair(IncomingString,Iter) != null;
}

function GetCharWithIter(IncomingString,Iter)
{
	var GSP = GetSurrogatePair(IncomingString,Iter);
	
	if(GSP !== null)
	{
		return [GSP,2];
	}
	else
	{
		return [IncomingString[Iter],1];
	}
}

function CreateUnicodeHandler()
{
	return {
					StringToList:[],
					SearchForString:function(IncomingString)
					{
						if(this.StringToList.length > 0)
						{
							for(var Iter = 0;Iter < this.StringToList.length;++Iter)
							{
								if(this.StringToList[Iter][0] == IncomingString)
								{
									return Iter;
								}
							}
						}
						return -1;
					},
					ConvertUnicodeStringToList:function(IncomingString)
					{
						var TempPair = null;
						var Collector = [];
						
						for(var Iter = 0;Iter < IncomingString.length;)
						{
							TempPair = GetCharWithIter(IncomingString,Iter);
							Collector.push(TempPair[0]);
							Iter += TempPair[1];
						}
						
						return Collector;
					},
					GetAddStringUnique:function(IncomingString)
					{
						var TempIter = this.SearchForString(IncomingString);
						if(TempIter > -1)
						{
							return this.StringToList[TempIter][1];
						}
						
						this.StringToList.push([IncomingString,this.ConvertUnicodeStringToList(IncomingString)]);
						return this.StringToList[this.StringToList.length-1][1];
					},
					ConvertListToUnicodeString:function(IncomingList)
					{
						var TempString = "";
						
						for(var Iter = 0; Iter < IncomingList.length;++Iter)
						{
							TempString += IncomingList[Iter];
						}
						
						return TempString;
					},
				};
}

var UnicodeHandler = CreateUnicodeHandler();
