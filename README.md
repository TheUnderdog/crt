**Censorship Resistance Tool (CRT)**

Most recent version is CRV8Min. Stands for Version 8 Minified. Code monkeys and those interested in seeing how it works on the underneath should use CRV8 instead.

**How To Use**

1. Download one of the above folders (or copy the entire GitLab directory for CRT)
2. Open the folder
3. Open the index.htm inside using the most recent browser you have

Older browsers may still be able to run portions of the code, however performance is not guaranteed.

**Features**

*Version 8*

- Quick emoji picker: geared towards vaccines
- Substition Table Selection: you can now enable/disable specific substitution tables
- Multi-character substitution: replaces specific multiple characters with single unicode characters that show the same letters
- Undo/Redo: Ctrl + Z/Ctrl + Y enabled on the textarea
- Unicode support (given JavaScript is extremely broken on handling unicode strings)
- Single character substitution mode: substitutes one character per word

**Test Notes**

CRV8 Tested.

Working for:

- Desktop Firefox 102
- Desktop Chrome

Broken for:

- Android 12
- Mobile Chrome
- Mobile Brave
- Mobile in general
